package ru.swayfarer.swl3.serializarion.serializer;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.observable.IObservable;
import ru.swayfarer.swl3.observable.Observables;
import ru.swayfarer.swl3.serializarion.serializer.provider.ISerializationProvider;

@Getter
@Setter
@Accessors(chain = true)
public class SerializationRules
{
    @Internal
    @NonNull
    public IObservable<ObjectSerializationEvent> eventDeserialization = newObservable();

    @Internal
    @NonNull
    public IObservable<ObjectSerializationEvent> eventSerialization = newObservable();

    public SerializationRules addProvider(ISerializationProvider provider, int priority)
    {
        eventDeserialization.subscribe().by(provider::read, priority);
        eventSerialization.subscribe().by(provider::write, priority);
        return this;
    }

    public SerializationRules addSerializer(IObjectSerializationRule rule, int priority)
    {
        eventSerialization.subscribe().by(rule, priority);
        return this;
    }

    public SerializationRules addDeserializer(IObjectSerializationRule rule, int priority)
    {
        eventSerialization.subscribe().by(rule, priority);
        return this;
    }

    public static IObservable<ObjectSerializationEvent> newObservable()
    {
        return Observables.<ObjectSerializationEvent>createObservable()
            .execution()
                    .useFilter((handle) -> !handle.getEventObject().isCompleted())

            .exceptions()
                    .configure((eh) -> {
                        eh.configure()
                            .rule().any()
                                .wrap(SerializationException.class)
                                .thanSkip()
                                .throwEx()
                        ;
                    })
        ;
    }
}
