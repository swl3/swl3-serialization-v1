package ru.swayfarer.swl3.serializarion.serializer.provider;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.exception.ExceptionsUtils;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.markers.Alias;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;
import ru.swayfarer.swl3.reflection.generic.GenericObject;
import ru.swayfarer.swl3.serializarion.property.ArrayTableProperty;
import ru.swayfarer.swl3.serializarion.property.SerializationCustomProperties;
import ru.swayfarer.swl3.serializarion.serializer.ObjectSerializationEvent;
import ru.swayfarer.swl3.serializarion.serializer.ObjectSerializationType;

import java.util.Collection;

@Getter
@Setter
@Accessors(chain = true)
public class CollectionSerializationProvider extends AbstractSerializationProvider
{
    @Internal
    @NonNull
    public ICollectionGenerator collectionGeneratorFun = (cl) -> null;

    public CollectionSerializationProvider()
    {
        this.setWriteRule(this::writeValue);
        this.setReadRule(this::readValue);
    }

    public void writeValue(ObjectSerializationEvent event)
    {
        SerializationCustomProperties.getSerializationRules(event)
                .ifPresent((serialization) -> {
                    var javaObject = event.getJavaObject();
                    if (javaObject instanceof Collection)
                    {
                        var collectionElementType = getCollectionElementType(event);
                        var javaCollection = (Collection<?>) javaObject;
                        var resultListPropertyObject = new ArrayTableProperty();
                        event.setCompleted(true);
                        event.setPropertyObject(resultListPropertyObject);

                        for (var collectionElement : javaCollection)
                        {
                            var elementSerializationType = ObjectSerializationType.builder()
                                    .genericType(collectionElementType.asGenericType())
                                    .javaType(collectionElementType.loadClass())
                                    .genericObject(collectionElementType.getChilds())
                                    .build()
                            ;

                            var collectionElementEvent = ObjectSerializationEvent.builder()
                                    .customProperties(event.getCustomProperties().copy())
                                    .javaObject(collectionElement)
                                    .type(elementSerializationType)
                                    .build()
                            ;

                            serialization.getEventSerialization().next(collectionElementEvent);

                            if (collectionElementEvent.isCompleted())
                            {
                                var elementPropertyObject = collectionElementEvent.getPropertyObject();

                                if (elementPropertyObject != null)
                                    resultListPropertyObject.add("", collectionElementEvent.getPropertyObject());
                            }
                        }
                    }
                })
        ;
    }

    public void readValue(ObjectSerializationEvent event)
    {
        SerializationCustomProperties.getSerializationRules(event)
                .ifPresent((serialization) -> {
                    var propertyObject = event.getPropertyObject();
                    if (propertyObject != null && propertyObject.isTable())
                    {
                        var table = propertyObject.asTable();
                        if (!table.hasKeys())
                        {
                            var javaCollectionType = event.getType().getJavaType();

                            if (Collection.class.isAssignableFrom(javaCollectionType))
                            {
                                var collectionElementType = getCollectionElementType(event);
                                var javaObject = event.getOrCreateJavaObject(() -> createCollectionInstance(javaCollectionType));

                                if (javaObject instanceof Collection)
                                {
                                    var javaCollection = (Collection) javaObject;
                                    event.setJavaObject(javaCollection);
                                    event.setCompleted(true);

                                    var tableEntries = table.getEntries();
                                    for (var tableEntry : tableEntries)
                                    {
                                        var elementSerializationType = ObjectSerializationType.builder()
                                                .genericType(collectionElementType.asGenericType())
                                                .javaType(collectionElementType.loadClass())
                                                .genericObject(collectionElementType.getChilds())
                                                .build()
                                        ;

                                        var collectionElementEvent = ObjectSerializationEvent.builder()
                                                .customProperties(event.getCustomProperties().copy())
                                                .propertyObject(tableEntry.getValue())
                                                .type(elementSerializationType)
                                                .build()
                                        ;

                                        serialization.getEventDeserialization().next(collectionElementEvent);

                                        if (collectionElementEvent.isCompleted())
                                        {
                                            javaCollection.add(collectionElementEvent.getJavaObject());
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
        );
    }

    private Collection<?> createCollectionInstance(Class<?> javaCollectionType)
    {
        if (collectionGeneratorFun == null)
            return null;

        return collectionGeneratorFun.apply(ReflectionsUtils.cast(javaCollectionType));
    }

    public GenericObject getCollectionElementType(ObjectSerializationEvent event)
    {
        var collectionGenerics = event.getType().getGenericObject();
        ExceptionsUtils.If(collectionGenerics.size() != 1, IllegalArgumentException.class, "Invalid collection generics! It must be a single generic, as List<String>! Not a", event.getType().getGenericType());
        return  collectionGenerics.first();
    }

    @Alias("IFunction1")
    public static interface ICollectionGenerator extends IFunction1<Class<? extends Collection<?>>, Collection<?>> {

    }
}
