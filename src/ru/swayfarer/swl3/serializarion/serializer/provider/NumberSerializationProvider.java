package ru.swayfarer.swl3.serializarion.serializer.provider;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.objects.ObjectsUtils;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;
import ru.swayfarer.swl3.serializarion.property.NumberProperty;
import ru.swayfarer.swl3.serializarion.serializer.ObjectSerializationEvent;

@Getter
@Setter
@Accessors(chain = true)
public class NumberSerializationProvider extends AbstractSerializationProvider
{
    @Internal
    @NonNull
    public ReflectionsUtils reflectionsUtils;

    public NumberSerializationProvider(ReflectionsUtils reflectionsUtils)
    {
        this.reflectionsUtils = reflectionsUtils;
        addTypeFilter((type) -> reflectionsUtils.types().isNumber(type.getJavaType()));
        setReadRule(this::readValue);
        setWriteRule(this::writeValue);
    }

    public void readValue(ObjectSerializationEvent event)
    {
        var propertyObject = event.getPropertyObject();
        if (propertyObject.isNum())
        {
            var numValue = propertyObject.asNum().getValue();
            readRequestedTypeNumber(event, numValue);
        }
    }

    public void writeValue(ObjectSerializationEvent event)
    {
        var javaObject = event.getJavaObject();
        var numberValue = (Number) null;

        if (javaObject instanceof Number)
        {
            numberValue = (Number) javaObject;
        }
        else if (javaObject instanceof CharSequence)
        {
            numberValue = Double.valueOf(String.valueOf(javaObject));
        }

        if (numberValue != null)
        {
            event.setPropertyObject(new NumberProperty().setValue(numberValue));
            event.setCompleted(true);
        }
    }

    public void readRequestedTypeNumber(ObjectSerializationEvent event, Number numValue)
    {
        var targetClassName = event.getType().getJavaType().getSimpleName();
        ObjectsUtils.getNumberValue(numValue, targetClassName)
                .ifPresent(javaObject -> {
                    event.setJavaObject(javaObject);
                    event.setCompleted(true);
                })
        ;
    }
}
