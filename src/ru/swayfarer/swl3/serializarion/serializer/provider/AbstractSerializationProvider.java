package ru.swayfarer.swl3.serializarion.serializer.provider;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.SneakyThrows;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.funs.chain.FunctionsChain1;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.objects.EqualsUtils;
import ru.swayfarer.swl3.serializarion.serializer.IObjectSerializationRule;
import ru.swayfarer.swl3.serializarion.serializer.ObjectSerializationEvent;
import ru.swayfarer.swl3.serializarion.serializer.ObjectSerializationType;

@Getter
@Setter
@Accessors(chain = true)
public class AbstractSerializationProvider implements ISerializationProvider
{
    @Internal
    @NonNull
    public FunctionsChain1<ObjectSerializationEvent, Boolean> eventFilter = new FunctionsChain1<ObjectSerializationEvent, Boolean>()
        .setFilterFun(Boolean.TRUE::equals)
    ;

    @Internal
    public IObjectSerializationRule readRule, writeRule;

    @SneakyThrows
    @Override
    public void read(ObjectSerializationEvent event)
    {
        if (!isEventPassed(event))
            return;

        if (readRule != null)
            readRule.handle(event);
    }

    @SneakyThrows
    @Override
    public void write(ObjectSerializationEvent event)
    {
        if (!isEventPassed(event))
            return;

        if (writeRule != null)
            writeRule.handle(event);
    }

    public boolean isEventPassed(ObjectSerializationEvent event)
    {
        return eventFilter == null || Boolean.TRUE.equals(eventFilter.apply(event));
    }

    public AbstractSerializationProvider addEventFilter(IFunction1<ObjectSerializationEvent, Boolean> fun)
    {
        if (fun != null)
        {
            eventFilter.add(fun);
        }

        return this;
    }

    public AbstractSerializationProvider addTypeFilter(Class<?>... classes)
    {
        return addTypeFilter((type) -> EqualsUtils.objectEqualsSome(type.getJavaType(), classes));
    }

    public AbstractSerializationProvider addTypeFilter(IFunction1<ObjectSerializationType, Boolean> fun)
    {
        if (fun != null)
        {
            eventFilter.add((objEvent) -> fun.apply(objEvent.getType()));
        }

        return this;
    }
}
