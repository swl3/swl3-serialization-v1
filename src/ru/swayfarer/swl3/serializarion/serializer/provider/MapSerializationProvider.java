package ru.swayfarer.swl3.serializarion.serializer.provider;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.exception.ExceptionsUtils;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.reflection.generic.GenericObject;
import ru.swayfarer.swl3.serializarion.property.ITableProperty.IWritableTableProperty;
import ru.swayfarer.swl3.serializarion.property.KeyedTableProperty;
import ru.swayfarer.swl3.serializarion.property.SerializationCustomProperties;
import ru.swayfarer.swl3.serializarion.serializer.ObjectSerializationEvent;
import ru.swayfarer.swl3.serializarion.serializer.ObjectSerializationType;
import ru.swayfarer.swl3.string.converters.StringConverters;

import java.util.Map;

@Getter
@Setter
@Accessors(chain = true)
public class MapSerializationProvider extends AbstractSerializationProvider
{
    @Internal
    @NonNull
    public StringConverters stringConverters = StringConverters.getInstance();

    @Internal
    @NonNull
    public IFunction1<Class<?>, Map<?, ?>> mapCreationFun = (cl) -> null;

    public MapSerializationProvider()
    {
        setWriteRule(this::writeValue);
        setReadRule(this::readValue);
    }

    public void readValue(ObjectSerializationEvent event)
    {
        if (!event.getPropertyObject().isTable())
            return;

        if (!event.getPropertyObject().asTable().hasKeys())
            return;

        SerializationCustomProperties.getSerializationRules(event)
                .ifPresent((serialization) -> {
                    var javaObject = event.getJavaObject();
                    if (javaObject instanceof Map)
                    {
                        var mapGenericTypes = getMapGenericTypes(event);

                        var table = event.getPropertyObject().asTable();
                        var javaMap = (Map) javaObject;
                        event.setCompleted(true);
                        event.setPropertyObject(table);

                        var mapValueSerializationType = ObjectSerializationType.builder()
                                .javaType(mapGenericTypes[1].loadClass())
                                .genericType(mapGenericTypes[1].asGenericType())
                                .genericObject(mapGenericTypes[1].getChilds())
                                .build()
                        ;

                        for (var tableEntry : table.getEntries())
                        {
                            ExceptionsUtils.IfNot(tableEntry.isKeyPresent(), IllegalArgumentException.class, "Non-keyed table entry in keyed table!");

                            var key = tableEntry.getName();
                            var mapValueEvent = ObjectSerializationEvent.builder()
                                    .type(mapValueSerializationType)
                                    .propertyObject(tableEntry.getValue())
                                    .customProperties(event.getCustomProperties().copy())
                                    .build()
                            ;

                            serialization.getEventDeserialization().next(mapValueEvent);

                            if (mapValueEvent.isCompleted() && mapValueEvent.getJavaObject() != null)
                            {
                                var convertedKey = stringConverters.convert(mapGenericTypes[0].loadClass(), key);
                                var javaEntryValue = mapValueEvent.getJavaObject();

                                if (convertedKey != null && javaEntryValue != null)
                                    javaMap.put(convertedKey, mapValueEvent.getJavaObject());
                            }
                        }
                    }
                }
        );
    }

    public void writeValue(ObjectSerializationEvent event)
    {
        SerializationCustomProperties.getSerializationRules(event)
            .ifPresent((serialization) -> {
                    var javaObject = event.getJavaObject();
                    if (javaObject instanceof Map)
                    {
                        var mapGenericTypes = getMapGenericTypes(event);
                        var rawTable = event.getOrCreatePropertyObject(KeyedTableProperty::new).asTable();

                        ExceptionsUtils.IfNot(
                                rawTable instanceof IWritableTableProperty,
                                IllegalArgumentException.class,
                                "Table property is not writable", rawTable
                        );

                        var table = (IWritableTableProperty) rawTable;
                        var javaMap = (Map<?, ?>) javaObject;
                        event.setCompleted(true);
                        event.setPropertyObject(rawTable);

                        var mapValueSerializationType = ObjectSerializationType.builder()
                                .javaType(mapGenericTypes[1].loadClass())
                                .genericType(mapGenericTypes[1].asGenericType())
                                .genericObject(mapGenericTypes[1].getChilds())
                                .build()
                        ;

                        for (var mapEntry : javaMap.entrySet())
                        {
                            var key = String.valueOf(mapEntry.getKey());
                            var value = mapEntry.getValue();

                            var mapValueEvent = ObjectSerializationEvent.builder()
                                    .javaObject(value)
                                    .type(mapValueSerializationType)
                                    .customProperties(event.getCustomProperties().copy())
                                    .build()
                            ;

                            serialization.getEventSerialization().next(mapValueEvent);

                            if (mapValueEvent.isCompleted() && mapValueEvent.getPropertyObject() != null)
                            {
                                table.add(key, mapValueEvent.getPropertyObject());
                            }
                        }
                    }
                }
            );
    }

    public GenericObject[] getMapGenericTypes(ObjectSerializationEvent event)
    {
        var javaObjectType = event.getType();
        ExceptionsUtils.IfNull(
                javaObjectType,
                IllegalArgumentException.class,
                "Serialization type must be set!"
        );

        var javaObjectGenerics = javaObjectType.getGenericObject();
        ExceptionsUtils.If(
                CollectionsSWL.isNullOrEmpty(javaObjectGenerics),
                IllegalArgumentException.class,
                "Map serialization event must has generics!"
        );

        ExceptionsUtils.IfNot(
                javaObjectGenerics.size() == 2,
                IllegalArgumentException.class,
                "Map serialization event generics must be a two entries, as Map<String, String>!"
        );

        return new GenericObject[] {javaObjectGenerics.get(0), javaObjectGenerics.get(1)};
    }
}
