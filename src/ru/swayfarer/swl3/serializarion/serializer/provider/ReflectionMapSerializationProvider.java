package ru.swayfarer.swl3.serializarion.serializer.provider;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.collections.map.ExtendedMap;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@Getter
@Setter
@Accessors(chain = true)
public class ReflectionMapSerializationProvider extends MapSerializationProvider
{
    @Internal
    @NonNull
    public ReflectionsUtils reflectionsUtils;

    @Internal
    @NonNull
    public Class<?> markerType;

    @Internal
    @NonNull
    public Set<Class<?>> availableTypes = new HashSet<>();

    @Internal
    @NonNull
    public ExtendedMap<Class<?>, IFunction0<Map<?, ?>>> mapCreators = new ExtendedMap<>();

    public ReflectionMapSerializationProvider(
            @NonNull Class<?> markerType,
            @NonNull ReflectionsUtils reflectionsUtils
    )
    {
        this.markerType = markerType;
        this.reflectionsUtils = reflectionsUtils;

        addTypeFilter((type) -> isAcceptsMarkerType(type.getJavaType()) || availableTypes.contains(type.getJavaType()));
        setMapCreationFun(this::createMap);
    }

    public ReflectionMapSerializationProvider addCreator(Class<?> type, IFunction0<Map<?, ?>> fun)
    {
        if (type != null && fun != null)
        {
            availableTypes.add(type);
            mapCreators.put(type, fun);
        }

        return this;
    }

    public boolean isAcceptsMarkerType(Class<?> cl)
    {
        return cl != null && markerType != null && markerType.isAssignableFrom(cl);
    }

    public Map<?, ?> createMap(Class<?> classOfMap)
    {
        if (mapCreators != null)
        {
            var registeredMapCreator = mapCreators.get(classOfMap);
            if (registeredMapCreator != null)
            {
                var createdMap = registeredMapCreator.apply();

                if (createdMap != null)
                {
                    return createdMap;
                }
            }
        }

        if (markerType != null && markerType.isAssignableFrom(classOfMap))
        {
            var reflectionCreatedMap = reflectionsUtils.constructors().newInstance(classOfMap).returnValue();
            if (reflectionCreatedMap != null && classOfMap.isAssignableFrom(reflectionCreatedMap.getClass()))
            {
                return (Map) reflectionCreatedMap;
            }
        }

        return null;
    }
}
