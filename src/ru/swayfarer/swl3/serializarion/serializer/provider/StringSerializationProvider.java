package ru.swayfarer.swl3.serializarion.serializer.provider;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.serializarion.property.StringProperty;
import ru.swayfarer.swl3.serializarion.serializer.ObjectSerializationEvent;

@Getter
@Setter
@Accessors(chain = true)
public class StringSerializationProvider extends AbstractSerializationProvider
{
    public StringSerializationProvider()
    {
        this.addTypeFilter(String.class, CharSequence.class);
        setReadRule(this::readValue);
        setWriteRule(this::writeValue);
    }

    public void readValue(ObjectSerializationEvent event)
    {
        var propertyObject = event.getPropertyObject();

        if (propertyObject.isString())
        {
            event.setJavaObject(propertyObject.asString().getValue());
            event.setCompleted(true);
        }
    }

    public void writeValue(ObjectSerializationEvent event)
    {
        event.setPropertyObject(new StringProperty().setValue(String.valueOf(event.getJavaObject())));
        event.setCompleted(true);
    }
}
