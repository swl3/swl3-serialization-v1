package ru.swayfarer.swl3.serializarion.serializer.provider;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;
import ru.swayfarer.swl3.serializarion.serializer.ReflectionSerializationRule;

@Getter
@Setter
@Accessors(chain = true)
public class ReflectionSerializationProvider extends AbstractSerializationProvider
{
    @Internal
    @NonNull
    public ReflectionsUtils reflectionsUtils;

    @Internal
    @NonNull
    public ReflectionSerializationRule reflectionReadRule;

    @Internal
    @NonNull
    public ReflectionSerializationRule reflectionWriteRule;

    public ReflectionSerializationProvider(ReflectionsUtils reflectionsUtils)
    {
        addTypeFilter((e) -> !e.getJavaType().isPrimitive());
        this.reflectionsUtils = reflectionsUtils;

        reflectionReadRule = new ReflectionSerializationRule(reflectionsUtils);
        reflectionReadRule.setForEachFieldFun(reflectionReadRule::readField);

        reflectionWriteRule = new ReflectionSerializationRule(reflectionsUtils);
        reflectionWriteRule.setForEachFieldFun(reflectionWriteRule::writeField);
        reflectionWriteRule.setInstanceCreatorFun((cl) -> reflectionsUtils.constructors().newInstance(cl));

        setReadRule(reflectionReadRule);
        setWriteRule(reflectionWriteRule);
    }
}
