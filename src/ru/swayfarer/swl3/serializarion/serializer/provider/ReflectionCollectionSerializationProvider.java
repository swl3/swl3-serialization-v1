package ru.swayfarer.swl3.serializarion.serializer.provider;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.collections.map.ExtendedMap;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@Accessors(chain = true)
public class ReflectionCollectionSerializationProvider extends CollectionSerializationProvider
{
    @Internal
    @NonNull
    public ReflectionsUtils reflectionsUtils;

    @Internal
    @NonNull
    public Set<Class<?>> availableClasses = new HashSet<>();

    @Internal
    @NonNull
    public Class<? extends Collection<?>> markerType;

    @Internal
    @NonNull
    public ExtendedMap<Class<?>, IFunction0<Collection<?>>> classToCollectionFuns = new ExtendedMap<>();

    public ReflectionCollectionSerializationProvider(
            Class<?> markerType,
            @NonNull ReflectionsUtils reflectionsUtils
    )
    {
        this.markerType = ReflectionsUtils.cast(markerType);
        this.reflectionsUtils = reflectionsUtils;
        addTypeFilter((type) -> isAcceptsMarkerType(type.getJavaType()) || availableClasses.contains(type.getJavaType()));
        setCollectionGeneratorFun(this::createCollection);
    }


    public boolean isAcceptsMarkerType(Class<?> cl)
    {
        return markerType != null && cl != null && markerType.isAssignableFrom(cl);
    }

    public Collection<?> createCollection(Class<?> collectionClass)
    {
        return classToCollectionFuns.getOptionalValue(collectionClass).map(IFunction0::apply).orElseGet(() -> {
            var result =  reflectionsUtils.constructors().newInstance(collectionClass).returnValue();
            return ReflectionsUtils.cast(result);
        });
    }

    public ReflectionCollectionSerializationProvider addCreator(Class<?> type, IFunction0<Collection<?>> collectionFun)
    {
        availableClasses.add(type);
        classToCollectionFuns.put(type, collectionFun);
        return this;
    }
}
