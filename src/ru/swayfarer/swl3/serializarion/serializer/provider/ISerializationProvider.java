package ru.swayfarer.swl3.serializarion.serializer.provider;

import ru.swayfarer.swl3.serializarion.serializer.ObjectSerializationEvent;

public interface ISerializationProvider
{
    public void read(ObjectSerializationEvent event);
    public void write(ObjectSerializationEvent event);
}
