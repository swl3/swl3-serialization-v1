package ru.swayfarer.swl3.serializarion.serializer.provider;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.serializarion.property.BooleanProperty;
import ru.swayfarer.swl3.serializarion.serializer.ObjectSerializationEvent;

@Getter
@Setter
@Accessors(chain = true)
public class BooleanSerializationProvider extends AbstractSerializationProvider
{
    public BooleanSerializationProvider()
    {
        addTypeFilter(Boolean.class, boolean.class);
        this.setReadRule(this::readValue);
        this.setWriteRule(this::writeValue);
    }

    public void readValue(ObjectSerializationEvent event)
    {
        var propertyObject = event.getPropertyObject();
        if (propertyObject.isBoolean())
        {
            event.setJavaObject(propertyObject.asBoolean().getValue());
            event.setCompleted(true);
        }
    }

    public void writeValue(ObjectSerializationEvent event)
    {
        var booleanValue = (Boolean) null;
        var javaObject = event.getJavaObject();

        if (javaObject instanceof Boolean)
            booleanValue = (Boolean) javaObject;

        else if (javaObject instanceof CharSequence)
        {
            var str = String.valueOf(javaObject);
            booleanValue = Boolean.valueOf(str);
        }

        if (booleanValue != null)
        {
            event.setPropertyObject(new BooleanProperty().setValue(booleanValue));
            event.setCompleted(true);
        }
    }
}
