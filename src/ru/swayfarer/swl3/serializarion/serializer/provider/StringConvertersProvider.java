package ru.swayfarer.swl3.serializarion.serializer.provider;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.serializarion.serializer.ObjectSerializationEvent;
import ru.swayfarer.swl3.string.converters.StringConverters;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@Accessors(chain = true)
public class StringConvertersProvider extends AbstractSerializationProvider
{
    @Internal
    @NonNull
    public Set<Class<?>> availableTypes = new HashSet<>();

    @Internal
    @NonNull
    public StringConverters stringConverters;

    public StringConvertersProvider()
    {
        this(StringConverters.getInstance());
    }

    public StringConvertersProvider(StringConverters stringConverters)
    {
        this.stringConverters = stringConverters;
        setReadRule(this::readValue);
        setWriteRule(this::writeValue);
    }

    public StringConvertersProvider acceptType(Class<?>... classes)
    {
        availableTypes.addAll(Arrays.asList(classes));
        return this;
    }

    public void writeValue(ObjectSerializationEvent event)
    {
        var javaObject = event.getJavaObject();

        if (javaObject != null)
        {
            event.setCompleted(true);
            event.setJavaObject(String.valueOf(javaObject));
        }
    }

    public void readValue(ObjectSerializationEvent event)
    {
        if (!event.getPropertyObject().isString())
            return;

        var result = stringConverters.convert(
                event.getType().getJavaType(),
                event.getPropertyObject().asString().getValue()
        );

        if (result != null)
        {
            event.setJavaObject(result);
            event.setCompleted(true);
        }
    }
}
