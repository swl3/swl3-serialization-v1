package ru.swayfarer.swl3.serializarion.serializer;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.markers.Internal;

import java.util.Collection;

@Getter
@Setter
@Accessors(chain = true)
public class CollectionSerializationRule implements IObjectSerializationRule
{
    @Internal
    @NonNull
    public IFunction1<Class<? extends Collection<?>>, Collection<?>> collectionGeneratorFun;

    @Override
    public void handle(ObjectSerializationEvent event) throws Throwable
    {
        
    }
}
