package ru.swayfarer.swl3.serializarion.serializer;

import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1NoR;

public interface IObjectSerializationRule extends IFunction1NoR<ObjectSerializationEvent>
{
    public void handle(ObjectSerializationEvent event) throws Throwable;

    @Override
    default void applyNoRUnsafe(ObjectSerializationEvent event) throws Throwable
    {
        handle(event);
    }
}
