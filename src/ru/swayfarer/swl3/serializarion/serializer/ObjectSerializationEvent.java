package ru.swayfarer.swl3.serializarion.serializer;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.experimental.SuperBuilder;
import ru.swayfarer.swl3.collections.map.ExtendedMap;
import ru.swayfarer.swl3.funs.ExtendedOptional;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.serializarion.property.IProperty;

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public class ObjectSerializationEvent
{
    @Builder.Default
    @Internal
    @NonNull
    public ExtendedMap<String, Object> customProperties = new ExtendedMap<>().synchronize();

    @Internal
    public Object javaObject;

    @Internal
    public IProperty propertyObject;

    @Internal
    @NonNull
    public ObjectSerializationType type;

    @Internal
    @NonNull
    public boolean completed;

    public ExtendedOptional<IProperty> getPropertyObjectOptional()
    {
        return ExtendedOptional.of(propertyObject);
    }

    public Object getOrCreateJavaObject(IFunction0<Object> fun)
    {
        if (fun != null && javaObject == null)
        {
            javaObject = fun.apply();
        }

        return javaObject;
    }

    public IProperty getOrCreatePropertyObject(IFunction0<IProperty> fun)
    {
        if (fun != null && propertyObject == null)
        {
            propertyObject = fun.apply();
        }

        return propertyObject;
    }
}
