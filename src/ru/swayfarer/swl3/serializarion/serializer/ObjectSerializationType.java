package ru.swayfarer.swl3.serializarion.serializer;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.experimental.SuperBuilder;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.reflection.generic.GenericObject;

import java.lang.reflect.Type;

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public class ObjectSerializationType
{
    @Internal
    @NonNull
    public Class<?> javaType;

    @Internal
    public Type genericType;

    @Builder.Default
    @Internal
    public ExtendedList<GenericObject> genericObject = new ExtendedList<>();
}
