package ru.swayfarer.swl3.serializarion.serializer;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.SneakyThrows;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.api.logger.ILogger;
import ru.swayfarer.swl3.api.logger.LogFactory;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1NoR;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction4NoR;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.observable.IObservable;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;
import ru.swayfarer.swl3.serializarion.property.KeyedTableProperty;
import ru.swayfarer.swl3.serializarion.property.SerializationCustomProperties;
import ru.swayfarer.swl3.serializarion.serializer.annotation.SerializePropertyName;
import ru.swayfarer.swl3.serializarion.serializer.annotation.SerializeSkip;

import java.lang.reflect.Field;

@Getter
@Setter
@Accessors(chain = true)
public class ReflectionSerializationRule implements IObjectSerializationRule
{
    public ILogger logger = LogFactory.getLogger();
    public boolean traceSerializationSkips = false;

    @Internal
    @NonNull
    public ReflectionsUtils reflectionsUtils;

    @Internal
    @NonNull
    public IFieldActionExecutor forEachFieldFun = this::readField;

    @Internal
    public IFunction1<Class<?>, Object> instanceCreatorFun;

    public ReflectionSerializationRule(
            @NonNull ReflectionsUtils reflectionsUtils
    )
    {
        this.reflectionsUtils = reflectionsUtils;
    }

    @Override
    public void handle(ObjectSerializationEvent event) throws Throwable
    {
        SerializationCustomProperties.getSerializationRules(event)
                .ifPresentOrElse(
                (serializers) -> {
                    forEachField(serializers, event, forEachFieldFun);
                },
                () -> {
                    if (traceSerializationSkips)
                    {
                        logger.warn("Skipping serialization event", event, "because no serialization rules found! Please, check your serialization settings!");
                    }
                }
        );
    }

    public void forEachField(
            SerializationRules serializationRules,
            ObjectSerializationEvent event,
            IFieldActionExecutor fun
    )
    {
        var javaObject = event.getOrCreateJavaObject(
                () -> instanceCreatorFun.apply(event.getType().getJavaType()
                )
        );

        if (javaObject != null)
        {
            reflectionsUtils.fields().stream(javaObject)
                    .not().filter(this::isNeedsToSkip)
                    .each((field) ->
                    {
                        fun.apply(serializationRules, event, javaObject, field);
                    })
            ;
        }
    }

    @SneakyThrows
    public void writeField(SerializationRules serializationRules, ObjectSerializationEvent event, Object javaObject, Field field)
    {
        var propertyName = resolvePropertyName(field);
        var table = event.getOrCreatePropertyObject(KeyedTableProperty::new)
                .asTable().asWritable().orElseThrow(() -> new IllegalStateException("Table is not writable!"));

        var eventHandler = serializationRules.getEventSerialization();
        var fieldObjectValue = field.get(javaObject);
        var fieldEvent = prepareSerializationEvent(event, field,
                eventHandler,
                (fe) -> fe
                        .setJavaObject(fieldObjectValue)
        );

        if (fieldEvent.isCompleted())
        {
            event.setCompleted(true);
            table.put(propertyName, fieldEvent.getPropertyObject());
        }
    }

    public void readField(SerializationRules serializationRules, ObjectSerializationEvent event, Object javaObject, Field field)
    {
        event.getPropertyObject().asTable().get(resolvePropertyName(field))
                .ifPresent(
                (propertyForField) ->
                {
                    var eventHandler = serializationRules.getEventDeserialization();
                    var fieldObjectValue = field.get(javaObject);
                    var fieldEvent = prepareSerializationEvent(event, field,
                            eventHandler,
                            (fe) -> fe
                                    .setPropertyObject(propertyForField)
                                    .setJavaObject(fieldObjectValue)
                    );

                    if (fieldEvent.isCompleted())
                    {
                        field.set(javaObject, fieldEvent.getJavaObject());
                        event.setCompleted(true);
                    }
                }
        );
    }

    private ObjectSerializationEvent prepareSerializationEvent(
            ObjectSerializationEvent parentEvent,
            Field field,
            IObservable<ObjectSerializationEvent> eventHandler,
            IFunction1NoR<ObjectSerializationEvent> eventCustomConfigurator
    )
    {
        var serializationType = ObjectSerializationType.builder()
                .javaType(field.getType())
                .genericType(field.getGenericType())
                .genericObject(reflectionsUtils.generics().of(field))
                .build()
        ;

        var fieldEvent = ObjectSerializationEvent.builder()
                .customProperties(parentEvent.getCustomProperties().copy())
                .type(serializationType)
                .build()
        ;

        eventCustomConfigurator.apply(fieldEvent);

        eventHandler.next(fieldEvent);
        return fieldEvent;
    }

    public boolean isNeedsToSkip(Field field)
    {
        return reflectionsUtils.annotations()
                .findAnnotation()
                    .ofType(SerializeSkip.class)
                    .in(field)
                    .get() != null
        ;
    }

    public String resolvePropertyName(Field field)
    {
        return reflectionsUtils.annotations()
                .findProperty()
                    .name("value")
                    .type(String.class)
                    .marker(SerializePropertyName.class)
                    .in(field)
                    .optional().orElseGet(field::getName)
        ;
    }

    public static interface IFieldActionExecutor extends IFunction4NoR<SerializationRules, ObjectSerializationEvent, Object, Field>
    {

    }
}
