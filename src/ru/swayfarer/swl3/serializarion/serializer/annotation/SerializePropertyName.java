package ru.swayfarer.swl3.serializarion.serializer.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface SerializePropertyName
{
    public String value();
}
