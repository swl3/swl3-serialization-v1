package ru.swayfarer.swl3.serializarion.module;

import ru.swayfarer.swl3.serialization.io.SerializationIO;
import ru.swayfarer.swl3.serialization.io.YamlSerializationIOProvider;

public class YamlModule extends SerializationModule
{
    @Override
    public void enrichIO(SerializationIO serializationIO)
    {
        serializationIO.addProvider(new YamlSerializationIOProvider());
    }
}
