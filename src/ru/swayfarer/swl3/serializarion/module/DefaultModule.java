package ru.swayfarer.swl3.serializarion.module;

import lombok.NonNull;
import lombok.var;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.collections.map.ExtendedMap;
import ru.swayfarer.swl3.io.file.FileSWL;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;
import ru.swayfarer.swl3.serializarion.serializer.SerializationRules;
import ru.swayfarer.swl3.serializarion.serializer.provider.BooleanSerializationProvider;
import ru.swayfarer.swl3.serializarion.serializer.provider.NumberSerializationProvider;
import ru.swayfarer.swl3.serializarion.serializer.provider.ReflectionCollectionSerializationProvider;
import ru.swayfarer.swl3.serializarion.serializer.provider.ReflectionMapSerializationProvider;
import ru.swayfarer.swl3.serializarion.serializer.provider.ReflectionSerializationProvider;
import ru.swayfarer.swl3.serializarion.serializer.provider.StringConvertersProvider;
import ru.swayfarer.swl3.serializarion.serializer.provider.StringSerializationProvider;
import ru.swayfarer.swl3.serialization.io.SerializationIO;

import java.io.File;
import java.time.format.DateTimeFormatter;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.UUID;

public class DefaultModule extends SerializationModule
{
    @Internal
    @NonNull
    public ReflectionsUtils reflectionsUtils;

    public DefaultModule()
    {
        this(ReflectionsUtils.getInstance());
    }

    public DefaultModule(ReflectionsUtils reflectionsUtils)
    {
        this.reflectionsUtils = reflectionsUtils;
    }

    @Override
    public void enrichIO(SerializationIO serializationIO)
    {
        super.enrichIO(serializationIO);
    }

    @Override
    public void enrichRules(SerializationRules serializationRules)
    {
        serializationRules.addProvider(new BooleanSerializationProvider(), 0);
        serializationRules.addProvider(new StringSerializationProvider(), 0);
        serializationRules.addProvider(new NumberSerializationProvider(reflectionsUtils), 0);

        var mapProvider = new ReflectionMapSerializationProvider(Map.class, reflectionsUtils);
        mapProvider.addCreator(Map.class, HashMap::new);
        mapProvider.addCreator(ExtendedMap.class, ExtendedMap::new);
        serializationRules.addProvider(mapProvider, 0);

        var listProvider = newCollectionProvider();
        listProvider.addCreator(List.class, ArrayList::new);
        listProvider.addCreator(ExtendedList.class, ExtendedList::new);
        serializationRules.addProvider(listProvider, 0);

        var setsProvider = newCollectionProvider();
        setsProvider.addCreator(Set.class, HashSet::new);
        serializationRules.addProvider(setsProvider, 0);

        var queuesProvider = newCollectionProvider();
        queuesProvider.addCreator(Queue.class, ArrayDeque::new);
        queuesProvider.addCreator(Deque.class, ArrayDeque::new);
        serializationRules.addProvider(queuesProvider, 0);

        var stringConverters = new StringConvertersProvider();
        stringConverters.addTypeFilter(UUID.class, FileSWL.class, File.class, DateTimeFormatter.class);
        serializationRules.addProvider(stringConverters, 0);

        var reflectionProvider = new ReflectionSerializationProvider(reflectionsUtils);
        serializationRules.addProvider(reflectionProvider, 500);

        super.enrichRules(serializationRules);
    }

    private ReflectionCollectionSerializationProvider newCollectionProvider()
    {
        return new ReflectionCollectionSerializationProvider(List.class, reflectionsUtils);
    }
}
