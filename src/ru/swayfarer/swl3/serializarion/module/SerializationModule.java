package ru.swayfarer.swl3.serializarion.module;

import ru.swayfarer.swl3.serializarion.serializer.SerializationRules;
import ru.swayfarer.swl3.serialization.io.SerializationIO;

public abstract class SerializationModule
{
    public void enrichRules(SerializationRules serializationRules)
    {

    }

    public void enrichIO(SerializationIO serializationIO)
    {

    }
}
