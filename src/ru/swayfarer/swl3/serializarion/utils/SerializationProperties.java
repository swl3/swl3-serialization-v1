package ru.swayfarer.swl3.serializarion.utils;

import lombok.NonNull;
import ru.swayfarer.swl3.funs.GeneratedFuns;
import ru.swayfarer.swl3.io.file.FileSWL;
import ru.swayfarer.swl3.io.link.RLUtils;
import ru.swayfarer.swl3.io.link.ResourceLink;
import ru.swayfarer.swl3.io.streams.DataOutStream;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;

public class SerializationProperties extends AbstractProperties<SerializationProperties, Boolean>
{

    @Internal
    @NonNull
    public GeneratedFuns.IFunction0<DataOutStream> dos;

    public SerializationProperties(@NonNull RLUtils rlUtils, @NonNull ReflectionsUtils reflectionsUtils)
    {
        super(rlUtils, reflectionsUtils);
    }

    @Override
    public SerializationProperties rlink(@NonNull ResourceLink rlinkObj)
    {
        this.resource = rlinkObj.getPath();
        this.dos = rlinkObj::out;

        return this;
    }

    @Override
    public SerializationProperties file(@NonNull FileSWL file)
    {
        this.resource = file.getAbsolutePath();
        this.dos = file::out;

        return this;
    }
}
