package ru.swayfarer.swl3.serializarion.utils;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.serializarion.property.IProperty;
import ru.swayfarer.swl3.serializarion.property.ITableProperty;

import java.util.ArrayList;
import java.util.LinkedHashMap;

@Getter
@Setter
@Accessors(chain = true)
public class PropertyToJavaConverter
{
    public Object toJavaObject(ITableProperty tableProperty)
    {
        if (tableProperty.hasKeys())
        {
            var map = new LinkedHashMap<String, Object>();
            for (var tableEntry : tableProperty.getEntries())
            {
                map.put(tableEntry.getName(), toJavaObject(tableEntry.getValue()));
            }

            return map;
        }
        else
        {
            var list = new ArrayList<>();
            for (var tableEntry : tableProperty.getEntries())
            {
                list.add(toJavaObject(tableEntry.getValue()));
            }

            return list;
        }
    }

    public Object toJavaObject(IProperty property)
    {
        if (property.isString())
            return property.asString().getValue();

        if (property.isNum())
            return property.asNum().getDouble();

        if (property.isBoolean())
            return property.asBoolean().getValue();

        if (property.isTable())
            return toJavaObject(property.asTable());

        return null;
    }
}
