package ru.swayfarer.swl3.serializarion.utils;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.exception.ExceptionsUtils;
import ru.swayfarer.swl3.funs.ExtendedOptional;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.io.link.RLUtils;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;
import ru.swayfarer.swl3.serializarion.module.DefaultModule;
import ru.swayfarer.swl3.serializarion.module.SerializationModule;
import ru.swayfarer.swl3.serializarion.module.YamlModule;
import ru.swayfarer.swl3.serializarion.property.SerializationCustomProperties;
import ru.swayfarer.swl3.serializarion.serializer.ObjectSerializationEvent;
import ru.swayfarer.swl3.serializarion.serializer.ObjectSerializationType;
import ru.swayfarer.swl3.serializarion.serializer.SerializationRules;
import ru.swayfarer.swl3.serialization.io.SerializationIO;

import java.lang.reflect.Type;

@Getter
@Setter
@Accessors(chain = true)
public class SerializationUtils
{
    public static SerializationUtils INSTANCE;

    static {
        INSTANCE = new SerializationUtils();
        INSTANCE.addModule(new DefaultModule());
        INSTANCE.addModule(new YamlModule());
    }

    @Internal
    @NonNull
    public RLUtils rlUtils;

    @Internal
    @NonNull
    public ReflectionsUtils reflectionsUtils;

    @Internal
    @NonNull
    public SerializationRules serializationRules = new SerializationRules();

    @Internal
    @NonNull
    public SerializationIO serializationIO = new SerializationIO();

    public SerializationUtils()
    {
        this(RLUtils.getInstance(), ReflectionsUtils.getInstance());
    }

    public SerializationUtils(@NonNull RLUtils rlUtils, @NonNull ReflectionsUtils reflectionsUtils)
    {
        this.rlUtils = rlUtils;
        this.reflectionsUtils = reflectionsUtils;
    }

    public <T> DeserializationProperties<ExtendedOptional<T>> reader()
    {
        var result = new DeserializationProperties<ExtendedOptional<T>>(rlUtils, reflectionsUtils);
        result.action = (e) -> ReflectionsUtils.cast(this.reader(e));
        return result;
    }

    public SerializationProperties writer()
    {
        var result = new SerializationProperties(rlUtils, reflectionsUtils);
        result.action = this::writer;
        return result;
    }

    public <T> ExtendedOptional<T> reader(DeserializationProperties<T> properties)
    {
        if (properties == null)
            return null;

        return serializationIO.readIO(properties.dis, properties.resource)
                .map((tableProperty) -> {
                    properties.propertyObject = tableProperty;
                    var serializationEvent = parseEvent(properties);
                    serializationRules.getEventDeserialization().next(serializationEvent);

                    if (serializationEvent.isCompleted())
                    {
                        return (T) serializationEvent.getJavaObject();
                    }

                    return null;
                })
        ;
    }

    public boolean writer(SerializationProperties properies)
    {
        ExceptionsUtils.IfNot(
                properies.propertyObject == null || properies.propertyObject.isTable(),
                IllegalArgumentException.class,
                "Property object must be a non-null table property, not a",
                properies.propertyObject
        );

        var serializationEvent = parseEvent(properies);
        serializationRules.getEventSerialization().next(serializationEvent);

        if (serializationEvent.isCompleted())
        {
            var propertyObject = serializationEvent.getPropertyObject();
            ExceptionsUtils.IfNot(propertyObject.isTable(), IllegalStateException.class, "Can't serialize not table property:", propertyObject);

            var tablePropertyObject = propertyObject.asTable();
            serializationIO.storeIO(properies.dos, properies.resource, tablePropertyObject);
            return true;
        }

        return false;
    }

    private ObjectSerializationEvent parseEvent(AbstractProperties<?, ?> properties)
    {
        var serializationType = parseType(properties.typeOfT);
        var result = ObjectSerializationEvent.builder()
                .type(serializationType)
                .propertyObject(properties.propertyObject)
                .javaObject(properties.javaObject)
                .propertyObject(properties.propertyObject)
                .build()
        ;

        SerializationCustomProperties.setSerializationRules(result, serializationRules);

        return result;
    }

    public ObjectSerializationType parseType(Type type)
    {
        var genericObject = reflectionsUtils.generics().parse(type);
        return ObjectSerializationType.builder()
                .javaType(genericObject.loadClass())
                .genericType(genericObject.asGenericType())
                .genericObject(genericObject.getChilds())
                .build()
        ;
    }

    public SerializationUtils addModule(SerializationModule module)
    {
        module.enrichIO(getSerializationIO());
        module.enrichRules(getSerializationRules());
        return this;
    }

    public static SerializationUtils getInstance()
    {
        return INSTANCE;
    }
}
