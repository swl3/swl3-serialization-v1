package ru.swayfarer.swl3.serializarion.utils;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.serializarion.property.ArrayTableProperty;
import ru.swayfarer.swl3.serializarion.property.BooleanProperty;
import ru.swayfarer.swl3.serializarion.property.IProperty;
import ru.swayfarer.swl3.serializarion.property.KeyedTableProperty;
import ru.swayfarer.swl3.serializarion.property.NumberProperty;
import ru.swayfarer.swl3.serializarion.property.StringProperty;

import java.util.Map;

@Getter
@Setter
@Accessors(chain = true)
public class JavaToPropertyConverter
{
    public IProperty toProperty(Object obj)
    {
        if (obj instanceof Boolean)
            return new BooleanProperty().setValue((Boolean) obj);
        else if (obj instanceof Number)
            return new NumberProperty().setValue((Number) obj);
        else if (obj instanceof CharSequence)
            return new StringProperty().setValue(String.valueOf(obj));
        else if (obj instanceof Map)
            return toTable((Map) obj);
        else if (obj.getClass().isArray())
            return toTable(CollectionsSWL.reflectionIterable(obj));
        else if (obj instanceof Iterable)
            return toTable((Iterable) obj);

        return null;
    }

    public IProperty toTable(Map<?, ?> map)
    {
        var tableProperty = new KeyedTableProperty();
        for (var entry : map.entrySet())
        {
            tableProperty.put(String.valueOf(entry.getKey()), toProperty( entry.getValue()));
        }

        return tableProperty;
    }

    public IProperty toTable(Iterable<?> iterable)
    {
        var tableProperty = new ArrayTableProperty();
        for (var element : iterable)
        {
            tableProperty.add("", toProperty(element));
        }

        return tableProperty;
    }
}
