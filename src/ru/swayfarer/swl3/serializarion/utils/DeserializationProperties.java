package ru.swayfarer.swl3.serializarion.utils;

import lombok.NonNull;
import ru.swayfarer.swl3.funs.GeneratedFuns;
import ru.swayfarer.swl3.io.file.FileSWL;
import ru.swayfarer.swl3.io.link.RLUtils;
import ru.swayfarer.swl3.io.link.ResourceLink;
import ru.swayfarer.swl3.io.streams.DataInStream;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;

import java.net.URL;

public class DeserializationProperties<Result_Type> extends AbstractProperties<DeserializationProperties<Result_Type>, Result_Type>
{
    @Internal
    @NonNull
    public GeneratedFuns.IFunction0<DataInStream> dis;

    public DeserializationProperties(@NonNull RLUtils rlUtils, @NonNull ReflectionsUtils reflectionsUtils)
    {
        super(rlUtils, reflectionsUtils);
    }

    @Override
    public DeserializationProperties<Result_Type> rlink(@NonNull ResourceLink rlinkObj)
    {
        this.resource = rlinkObj.getPath();
        this.dis = rlinkObj::in;
        return this;
    }

    @Override
    public DeserializationProperties<Result_Type> file(@NonNull FileSWL file)
    {
        this.resource = file.getAbsolutePath();
        this.dis = file::in;
        return this;
    }

    public DeserializationProperties<Result_Type> url(String url)
    {
        return rlink("u:" + url);
    }

    public DeserializationProperties<Result_Type> url(URL url)
    {
        return rlink("u:" + url.toExternalForm());
    }
}
