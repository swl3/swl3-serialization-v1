package ru.swayfarer.swl3.serializarion.utils;

import lombok.NonNull;
import ru.swayfarer.swl3.funs.GeneratedFuns;
import ru.swayfarer.swl3.io.file.FileSWL;
import ru.swayfarer.swl3.io.link.RLUtils;
import ru.swayfarer.swl3.io.link.ResourceLink;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;
import ru.swayfarer.swl3.serializarion.property.IProperty;

import java.io.File;
import java.lang.reflect.Type;

public abstract class AbstractProperties<Return_Type, Action_Result_Type>
{
    @Internal
    @NonNull
    public RLUtils rlUtils;

    @Internal
    @NonNull
    public ReflectionsUtils reflectionsUtils;

    @Internal
    @NonNull
    public String resource;

    @Internal
    @NonNull
    public Type typeOfT;

    @Internal
    @NonNull
    public Object javaObject;

    @Internal
    @NonNull
    public IProperty propertyObject;

    @Internal
    @NonNull
    public GeneratedFuns.IFunction1<Return_Type, Action_Result_Type> action = (e) -> null;

    public AbstractProperties(@NonNull RLUtils rlUtils, @NonNull ReflectionsUtils reflectionsUtils)
    {
        this.rlUtils = rlUtils;
        this.reflectionsUtils = reflectionsUtils;
    }

    public Return_Type type(@NonNull Type type)
    {
        this.typeOfT = type;
        return (Return_Type) this;
    }

    public Return_Type javaObject(Object obj)
    {
        if (obj != null)
        {
            if (typeOfT == null)
            {
                typeOfT = obj.getClass();
            }

            this.javaObject = obj;
        }

        return (Return_Type) this;
    }

    public Return_Type resource(String res)
    {
        if (res != null)
        {
            this.resource = res;
        }

        return (Return_Type) this;
    }

    public Return_Type propertyObject(IProperty property)
    {
        this.propertyObject = property;
        return (Return_Type) this;
    }

    public abstract Return_Type rlink(@NonNull ResourceLink rlinkObj);

    public abstract Return_Type file(@NonNull FileSWL file);

    public Return_Type rlink(@NonNull String rlink)
    {
        return rlink(rlUtils.createLink(rlink));
    }

    public Return_Type file(@NonNull String filepath)
    {
        return file(FileSWL.of(filepath));
    }

    public Return_Type file(@NonNull File file)
    {
        return file(FileSWL.of(file));
    }

    public Action_Result_Type run()
    {
        return action.apply((Return_Type) this);
    }
}
