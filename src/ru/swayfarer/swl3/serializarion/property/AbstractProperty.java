package ru.swayfarer.swl3.serializarion.property;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.collections.map.ExtendedMap;
import ru.swayfarer.swl3.exception.ExceptionsUtils;
import ru.swayfarer.swl3.markers.Internal;

@Getter
@Setter
@Accessors(chain = true)
public abstract class AbstractProperty implements IProperty, PropertyTypes
{
    @Internal
    @NonNull
    public ExtendedMap<String, Object> customProperties = new ExtendedMap<>().synchronize();

    @Override
    public boolean isString()
    {
        return false;
    }

    @Override
    public boolean isNum()
    {
        return false;
    }

    @Override
    public boolean isBoolean()
    {
        return false;
    }

    @Override
    public boolean isTable()
    {
        return false;
    }

    @Override
    public boolean hasValue()
    {
        return false;
    }

    @Override
    public StringProperty asString()
    {
        throwUnsupported("string");
        return null;
    }

    @Override
    public ITableProperty asTable()
    {
        throwUnsupported("table");
        return null;
    }

    @Override
    public IBooleanProperty asBoolean()
    {
        throwUnsupported("boolean");
        return null;
    }

    @Override
    public INumberProperty asNum()
    {
        throwUnsupported("num");
        return null;
    }

    @Override
    public <T> IValueProperty<T> asValue()
    {
        throwUnsupported("value");
        return null;
    }

    @Internal
    public void throwUnsupported(String representation)
    {
        ExceptionsUtils.unsupportedOperation("Property", this, "can't be represented as", representation, "!");
    }
}
