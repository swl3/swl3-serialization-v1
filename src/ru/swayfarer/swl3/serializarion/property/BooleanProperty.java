package ru.swayfarer.swl3.serializarion.property;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.objects.ObjectsUtils;
import ru.swayfarer.swl3.string.dynamic.DynamicString;

@Getter
@Setter
@Accessors(chain = true)
public class BooleanProperty extends AbstractValueProperty<Boolean> implements IBooleanProperty
{
    public BooleanProperty()
    {
        setValue(false);
    }

    @Override
    public String getType()
    {
        return propertyTypeBoolean;
    }

    @Override
    public boolean isBoolean()
    {
        return true;
    }

    @Override
    public IBooleanProperty asBoolean()
    {
        return this;
    }

    @Override
    public void toFancyString(DynamicString buf, int indent)
    {
        buf.append("boolean: ");
        buf.append(getValue());
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
            return true;

        if (!(o instanceof IBooleanProperty))
            return false;

        var that = (IBooleanProperty) o;
        return value == that.getValue();
    }

    @Override
    public int hashCode()
    {
        return ObjectsUtils.hash(value);
    }
}
