package ru.swayfarer.swl3.serializarion.property;

import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.funs.ExtendedOptional;

public interface ITableProperty extends IProperty, Iterable<ITableProperty.IEntry>
{
    public boolean hasKeys();
    public ExtendedList<IEntry> getEntries();
    public ExtendedOptional<IProperty> get(String key);
    public ExtendedList<IProperty> getAll(String key);

    public default boolean isWritable()
    {
        return false;
    }

    public ExtendedOptional<IWritableTableProperty> asWritable();

    @Override
    default boolean isTable()
    {
        return true;
    }

    public static interface IWritableTableProperty extends ITableProperty{
        public IWritableTableProperty add(String key, IProperty value);
        public IWritableTableProperty put(String key, IProperty value);
        public IWritableTableProperty remove(String key);
        public IWritableTableProperty remove(String key, IProperty value);

        public default boolean isWritable()
        {
            return true;
        }

        public default ExtendedOptional<IWritableTableProperty> asWritable()
        {
            return ExtendedOptional.of(this);
        }
    }

    public static interface IEntry {
        public String getName();
        public IProperty getValue();
        public boolean isKeyPresent();
    }
}
