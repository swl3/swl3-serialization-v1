package ru.swayfarer.swl3.serializarion.property;

public interface INumberProperty extends IValueProperty<Number>
{
    public Number getValue();

    /*
    --------------------------------------------------------------------
                                  Defaults
    --------------------------------------------------------------------
     */

    public default short getByte()
    {
        return getValue().byteValue();
    }

    public default short getShort()
    {
        return getValue().shortValue();
    }

    public default int getInt()
    {
        return getValue().intValue();
    }

    public default long getLong()
    {
        return getValue().longValue();
    }

    public default float getFloat()
    {
        return getValue().floatValue();
    }

    public default double getDouble()
    {
        return getValue().doubleValue();
    }

    @Override
    default boolean isNum()
    {
        return true;
    }
}
