package ru.swayfarer.swl3.serializarion.property;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.objects.EqualsUtils;
import ru.swayfarer.swl3.objects.ObjectsUtils;
import ru.swayfarer.swl3.string.dynamic.DynamicString;

import java.util.Objects;

@Getter
@Setter
@Accessors(chain = true)
public class NumberProperty extends AbstractValueProperty<Number> implements INumberProperty
{
    public NumberProperty()
    {
        setValue(0);
    }

    @Override
    public String getType()
    {
        return propertyTypeNumber;
    }

    @Override
    public INumberProperty asNum()
    {
        return this;
    }

    @Override
    public boolean isNum()
    {
        return true;
    }

    @Override
    public void toFancyString(DynamicString buf, int indent)
    {
        buf.append("num: ");
        buf.append(getValue());
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
            return true;
        if (!(o instanceof INumberProperty))
            return false;
        var that = (INumberProperty) o;

        return EqualsUtils.objectEquals(value, that.getValue());
    }

    @Override
    public int hashCode()
    {
        return ObjectsUtils.hash(value);
    }
}
