package ru.swayfarer.swl3.serializarion.property;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.objects.EqualsUtils;
import ru.swayfarer.swl3.objects.ObjectsUtils;
import ru.swayfarer.swl3.string.dynamic.DynamicString;

@Getter
@Setter
@Accessors(chain = true)
public class StringProperty extends AbstractValueProperty<String> implements IStringProperty
{
    public StringProperty()
    {
        setValue("");
    }

    @Override
    public String getType()
    {
        return propertyTypeString;
    }

    @Override
    public void toFancyString(DynamicString buf, int indent)
    {
        buf.append("str: ");
        buf.append(getValue());
    }

    @Override
    public boolean isString()
    {
        return true;
    }

    @Override
    public StringProperty asString()
    {
        return this;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
            return true;
        if (!(o instanceof IStringProperty))
            return false;
        var that = (IStringProperty) o;

        return EqualsUtils.objectEquals(value, that.getValue());
    }

    @Override
    public int hashCode()
    {
        return ObjectsUtils.hash(value);
    }
}
