package ru.swayfarer.swl3.serializarion.property;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.markers.Internal;

@Getter
@Setter
@Accessors(chain = true)
public abstract class AbstractValueProperty<Value_Type> extends AbstractProperty implements IValueProperty<Value_Type>
{
    @Internal
    public Value_Type value;

    @Override
    public Value_Type getValue()
    {
        return value;
    }

    @Override
    public <T> IValueProperty<T> asValue()
    {
        return (IValueProperty<T>) this;
    }

    @Override
    public boolean hasValue()
    {
        return true;
    }
}
