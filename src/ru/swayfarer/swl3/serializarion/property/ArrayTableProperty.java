package ru.swayfarer.swl3.serializarion.property;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.funs.ExtendedOptional;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;
import ru.swayfarer.swl3.string.StringUtils;
import ru.swayfarer.swl3.string.dynamic.DynamicString;

import java.util.Iterator;
import java.util.Objects;

@Getter
@Setter
@Accessors(chain = true)
public class ArrayTableProperty extends AbstractProperty implements ITableProperty.IWritableTableProperty
{
    @Internal
    @NonNull
    public ExtendedList<ArrayEntry> entries = new ExtendedList<>().synchronyzed();

    @Override
    public String getType()
    {
        return propertyTypeTable;
    }

    @Override
    public boolean hasKeys()
    {
        return false;
    }

    @Override
    public ExtendedList<IEntry> getEntries()
    {
        return ReflectionsUtils.cast(entries.copy());
    }

    @Override
    public ExtendedOptional<IProperty> get(String key)
    {
        var validIndex = getValidIndex(key);
        if (validIndex.isPresent())
        {
            return ExtendedOptional.of(entries.get(validIndex.get()).getValue());
        }

        return ExtendedOptional.empty();
    }

    @Override
    public ExtendedList<IProperty> getAll(String key)
    {
        return get(key)
                .map(CollectionsSWL::list)
                .orElseGet(ExtendedList::new)
        ;
    }

    @Override
    public IWritableTableProperty add(String key, IProperty value)
    {
        entries.add(new ArrayEntry().setValue(value));
        return this;
    }

    @Override
    public IWritableTableProperty put(String key, IProperty value)
    {
        entries.add(new ArrayEntry().setValue(value));
        return this;
    }

    @Override
    public IWritableTableProperty remove(String key)
    {
        var validIndex = getValidIndex(key);
        if (validIndex.isPresent())
        {
            entries.remove(validIndex.get().intValue());
        }

        return this;
    }

    @Override
    public IWritableTableProperty remove(String key, IProperty value)
    {
        return remove(key);
    }

    public ExtendedOptional<Integer> getValidIndex(String key)
    {
        if (StringUtils.isInteger(key))
        {
            var num = Integer.parseInt(key);
            if (entries.size() > num)
            {
                return ExtendedOptional.of(num);
            }
        }

        return ExtendedOptional.empty();
    }

    @Override
    public ITableProperty asTable()
    {
        return this;
    }

    @Override
    public void toFancyString(DynamicString buf, int indent)
    {
        buf.append("array: [");
        buf.newLine();

        indent ++;

        var fisrt = true;

        for (var entry : entries)
        {
            if (!fisrt)
            {
                buf.append(",").newLine();
            }

            buf.indent(indent);

            fisrt = false;

            entry.getValue().toFancyString(buf, indent);
        }

        indent --;
        buf.newLine();
        buf.indent(indent);
        buf.append("]");
    }

    @Override
    public boolean isTable()
    {
        return true;
    }

    @Override
    public Iterator<IEntry> iterator()
    {
        var iterator = entries.copy().iterator();
        return ReflectionsUtils.cast(iterator);
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
            return true;

        if (!(o instanceof ITableProperty))
            return false;


        var iEntries = (ITableProperty) o;

        if (iEntries.hasKeys() != hasKeys())
            return false;

        return Objects.equals(entries, iEntries.getEntries());
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(entries);
    }

    @Getter
    @Setter
    @Accessors(chain = true)
    public static class ArrayEntry implements IEntry {
        public String name;
        public IProperty value;

        @Override
        public boolean isKeyPresent()
        {
            return false;
        }
    }
}
