package ru.swayfarer.swl3.serializarion.property;

public interface PropertyTypes
{
    public String propertyTypeString = "swl3/serialization-v1/string";
    public String propertyTypeNumber = "swl3/serialization-v1/number";
    public String propertyTypeTable = "swl3/serialization-v1/table";
    public String propertyTypeBoolean = "swl3/serialization-v1/boolean";
}
