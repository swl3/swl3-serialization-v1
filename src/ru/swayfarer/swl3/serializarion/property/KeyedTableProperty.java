package ru.swayfarer.swl3.serializarion.property;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.experimental.SuperBuilder;
import lombok.var;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.collections.map.ExtendedMap;
import ru.swayfarer.swl3.funs.ExtendedOptional;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.objects.EqualsUtils;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;
import ru.swayfarer.swl3.serializarion.property.ITableProperty.IWritableTableProperty;
import ru.swayfarer.swl3.string.dynamic.DynamicString;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Objects;

@Getter
@Setter
@Accessors(chain = true)
public class KeyedTableProperty extends AbstractProperty implements IWritableTableProperty
{
    @Internal
    @NonNull
    public ExtendedMap<String, ExtendedList<KeyedEntry>> entries = new ExtendedMap<>().synchronize();

    @Override
    public boolean hasKeys()
    {
        return true;
    }

    @Override
    public ExtendedList<IEntry> getEntries()
    {
        var result = entries.exStream()
                .map(Map.Entry::getValue)
                .flatMap(Collection::stream)
                .toExList()
        ;

        return ReflectionsUtils.cast(result);
    }

    @Override
    public ExtendedOptional<IProperty> get(String key)
    {
        var entries = getForKey(key);
        return entries == null ? ExtendedOptional.empty() : ExtendedOptional.of(entries.first().getValue());
    }

    @Override
    public ExtendedList<IProperty> getAll(String key)
    {
        var result = getForKey(key);
        if (result == null)
            return new ExtendedList<>();

        return result.exStream()
                .map(ITableProperty.IEntry::getValue)
                .toExList()
        ;
    }

    @Override
    public IWritableTableProperty add(String key, IProperty value)
    {
        getOrCreateForKey(key).add(KeyedEntry.builder()
                .name(key)
                .value(value)
                .build()
        );

        return this;
    }

    @Override
    public IWritableTableProperty put(String key, IProperty value)
    {
        getOrCreateForKey(key).setAll(KeyedEntry.builder()
                .name(key)
                .value(value)
                .build()
        );

        return this;
    }

    @Override
    public IWritableTableProperty remove(String key)
    {
        entries.remove(key);
        return this;
    }

    @Override
    public IWritableTableProperty remove(String key, IProperty value)
    {
        var entries = getForKey(key);
        if (entries != null)
        {
            entries.removeIf((entry) -> EqualsUtils.objectEquals(entry.getValue(), value));
        }
        return null;
    }

    @Override
    public ITableProperty asTable()
    {
        return this;
    }

    @Override
    public boolean isTable()
    {
        return true;
    }

    @Override
    public void toFancyString(DynamicString buf, int indent)
    {
        buf.append("table: {");
        buf.newLine();

        indent ++;

        var first = true;

        for (var entry : getEntries())
        {
            if (!first)
            {
                buf.append(",").newLine();
            }

            buf.indent(indent);

            first = false;

            buf.append(entry.getName());
            buf.append(" = ");
            entry.getValue().toFancyString(buf, indent);
        }

        indent --;
        buf.newLine();
        buf.indent(indent);
        buf.append("}");
    }

    @Override
    public Iterator<IEntry> iterator()
    {
        return getEntries().iterator();
    }

    @Override
    public String getType()
    {
        return propertyTypeTable;
    }

    public ExtendedList<KeyedEntry> getForKey(String key)
    {
        return entries.get(key);
    }

    public ExtendedList<KeyedEntry> getOrCreateForKey(String key)
    {
        return entries.getOrCreate(key, () -> new ExtendedList<>().synchronyzed());
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
            return true;

        if (!(o instanceof KeyedTableProperty))
            return false;

        var iEntries = (ITableProperty) o;

        if (iEntries.hasKeys() != hasKeys())
            return false;

        return Objects.equals(getEntries(), iEntries.getEntries());
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(entries);
    }

    @Getter
    @Setter
    @Accessors(chain = true)
    @NoArgsConstructor
    @AllArgsConstructor
    @SuperBuilder
    public static class KeyedEntry implements ITableProperty.IEntry {

        @NonNull
        @Internal
        public String name;

        @NonNull
        @Internal
        public IProperty value;

        @Override
        public boolean isKeyPresent()
        {
            return true;
        }

        @Override
        public boolean equals(Object o)
        {
            if (this == o)
                return true;
            if (!(o instanceof ITableProperty.IEntry))
                return false;

            var that = (ITableProperty.IEntry) o;
            return Objects.equals(name, that.getName()) && Objects.equals(value, that.getValue());
        }

        @Override
        public int hashCode()
        {
            return Objects.hash(name, value);
        }
    }
}
