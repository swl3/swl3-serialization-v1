package ru.swayfarer.swl3.serializarion.property;

public interface IStringProperty extends IValueProperty<String>
{
    public String getValue();

    @Override
    default boolean isString()
    {
        return true;
    }
}
