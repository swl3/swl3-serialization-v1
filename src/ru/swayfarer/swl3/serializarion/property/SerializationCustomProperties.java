package ru.swayfarer.swl3.serializarion.property;

import ru.swayfarer.swl3.funs.ExtendedOptional;
import ru.swayfarer.swl3.serializarion.serializer.ObjectSerializationEvent;
import ru.swayfarer.swl3.serializarion.serializer.SerializationRules;

public interface SerializationCustomProperties
{
    public String customPropertySerializationRules = "serializationRules";
    public String customPropertyAssociatedType = "associatedType";
    public String customPropertyComment = "associatedType";

    public static ExtendedOptional<Class<?>> getAssociatedType(IProperty property)
    {
        return property.getCustomProperties().getOptionalValue(customPropertyAssociatedType);
    }

    public static void setAssociatedType(IProperty property, Class<?> type)
    {
        property.getCustomProperties().put(customPropertyAssociatedType, type);
    }

    public static ExtendedOptional<String> getComment(IProperty property)
    {
        return property.getCustomProperties().getOptionalValue(customPropertyComment);
    }

    public static void setComment(IProperty property, String comment)
    {
        property.getCustomProperties().put(customPropertyComment, comment);
    }

    public static ExtendedOptional<SerializationRules> getSerializationRules(ObjectSerializationEvent event)
    {
        return event.getCustomProperties().getOptionalValue(customPropertySerializationRules);
    }

    public static void setSerializationRules(ObjectSerializationEvent event, SerializationRules rules)
    {
        event.getCustomProperties().put(customPropertySerializationRules, rules);
    }
}
