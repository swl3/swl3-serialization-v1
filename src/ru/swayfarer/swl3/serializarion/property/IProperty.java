package ru.swayfarer.swl3.serializarion.property;

import lombok.var;
import ru.swayfarer.swl3.collections.map.ExtendedMap;
import ru.swayfarer.swl3.string.dynamic.DynamicString;

public interface IProperty
{
    public ExtendedMap<String, Object> getCustomProperties();
    public String getType();

    public boolean isString();
    public boolean isNum();
    public boolean isBoolean();
    public boolean isTable();
    public boolean hasValue();

    public StringProperty asString();
    public ITableProperty asTable();
    public INumberProperty asNum();
    public IBooleanProperty asBoolean();
    public <T> IValueProperty<T> asValue();

    public void toFancyString(DynamicString buf, int indent);
    public default String toFancyString()
    {
        var buf = new DynamicString();
        buf.setIndentSpacesCount(2);
        toFancyString(buf, 0);
        return buf.toString();
    }
}
