package ru.swayfarer.swl3.serializarion.property;

public interface IBooleanProperty extends IProperty, IValueProperty<Boolean>
{
    public Boolean getValue();

    @Override
    default boolean isBoolean()
    {
        return true;
    }
}
