package ru.swayfarer.swl3.serializarion.property;

public interface IValueProperty<Value_Type> extends IProperty
{
    public Value_Type getValue();
}
