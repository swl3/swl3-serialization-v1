package ru.swayfarer.swl3.serialization.io;

import ru.swayfarer.swl3.io.streams.DataInStream;
import ru.swayfarer.swl3.io.streams.DataOutStream;

public interface IPropertyIOProvider
{
    public void read(PropertyIOEvent<DataInStream> event);
    public void write(PropertyIOEvent<DataOutStream> event);
}
