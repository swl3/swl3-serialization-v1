package ru.swayfarer.swl3.serialization.io;

import ru.swayfarer.swl3.io.streams.DataInStream;
import ru.swayfarer.swl3.io.streams.DataOutStream;

public class MapBasedSerializationIO implements IPropertyIOProvider
{
    @Override
    public void read(PropertyIOEvent<DataInStream> event)
    {

    }

    @Override
    public void write(PropertyIOEvent<DataOutStream> event)
    {

    }
}
