package ru.swayfarer.swl3.serialization.io;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.io.path.PathHelper;
import ru.swayfarer.swl3.io.streams.DataInStream;
import ru.swayfarer.swl3.io.streams.DataOutStream;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.string.FilteringList;

@Getter
@Setter
@Accessors(chain = true)
public abstract class ExtensionSensitiveIOProvider implements IPropertyIOProvider
{
    @Internal
    @NonNull
    public FilteringList resourceFilters = new FilteringList();

    @Internal
    @Deprecated
    @Override
    public void read(PropertyIOEvent<DataInStream> event)
    {
        if (isMatches(event))
            readValue(event);
    }

    @Internal
    @Deprecated
    @Override
    public void write(PropertyIOEvent<DataOutStream> event)
    {
        if (isMatches(event))
            writeValue(event);
    }

    public abstract void readValue(PropertyIOEvent<DataInStream> event);
    public abstract void writeValue(PropertyIOEvent<DataOutStream> event);

    public boolean isMatches(PropertyIOEvent<?> event)
    {
        return resourceFilters.isMatches(event.getResourceName());
    }
}
