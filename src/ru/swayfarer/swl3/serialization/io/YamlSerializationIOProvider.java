package ru.swayfarer.swl3.serialization.io;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.exception.ExceptionsUtils;
import ru.swayfarer.swl3.io.streams.DataInStream;
import ru.swayfarer.swl3.io.streams.DataOutStream;
import ru.swayfarer.swl3.io.streams.StreamsUtils;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.serializarion.property.ITableProperty;
import ru.swayfarer.swl3.serializarion.property.KeyedTableProperty;
import ru.swayfarer.swl3.serializarion.utils.JavaToPropertyConverter;
import ru.swayfarer.swl3.serializarion.yaml.ho.yaml.YamlDecoder;
import ru.swayfarer.swl3.string.StringUtils;
import ru.swayfarer.swl3.string.dynamic.DynamicString;

import java.io.InputStream;
import java.io.StringReader;

import static ru.swayfarer.swl3.serializarion.property.SerializationCustomProperties.getComment;

@Getter
@Setter
@Accessors(chain = true)
public class YamlSerializationIOProvider extends ExtensionSensitiveIOProvider
{
    @Internal
    @NonNull
    public String equalsSymbol = ": ";

    @Internal
    @NonNull
    public String listSymbol = "- ";

    @Internal
    @NonNull
    public String commentSymbol = "# ";

    @Internal
    @NonNull
    public int indent = 2;

    @Internal
    @NonNull
    public JavaToPropertyConverter javaToPropertyConverter = new JavaToPropertyConverter();

    public YamlSerializationIOProvider()
    {
        this.resourceFilters.add("i:mask:*.yml");
        this.resourceFilters.add("i:mask:*.yaml");
    }

    @Override
    public void readValue(PropertyIOEvent<DataInStream> event)
    {
        event.setTablePropertyObject(readYamlTable(event.getDataStream().apply()));
        event.setCompleted(true);
    }

    @Override
    public void writeValue(PropertyIOEvent<DataOutStream> event)
    {
        var os = event.getDataStream().apply();
        os.writeString(writeYamlTableLayer(event.getTablePropertyObject()));
        os.flush();
        os.close();

        event.setCompleted(true);
    }

    public String writeYamlTableLayer(ITableProperty table)
    {
        var buffer = new DynamicString();
        buffer.indentSpacesCount = indent;
        writeYamlTableLayer(table, 0, buffer);
        buffer.newLine();
        return buffer.toString();
    }

    public void writeYamlTableLayer(ITableProperty tableProperty, int currentIndent, DynamicString buffer)
    {
        int originalIndent = currentIndent;
        for (var tableEntry : tableProperty.getEntries())
        {
            var propertyObject = tableEntry.getValue();

            if (propertyObject.isTable() && propertyObject.asTable().getEntries().isEmpty())
                continue;

            currentIndent = originalIndent;
            buffer.indent(currentIndent);

            var comment = getComment(propertyObject).orNull();
            if (!StringUtils.isBlank(comment))
            {
                var lines = comment.split("\n");

                for (var line : lines)
                {
                    buffer.append(commentSymbol);
                    buffer.append(line);

                    buffer.newLine();
                    buffer.indent(currentIndent);
                }
            }

            if (tableProperty.hasKeys())
            {
                String key = tableEntry.getName();

                ExceptionsUtils.If(
                        key.contains(": "),
                        UnsupportedOperationException.class,
                        "Can't write key that contains ': ' to yaml syntax! It's system word. Please, don't use that key or try another format!"
                );

                buffer.append(key);
                buffer.append(equalsSymbol);
            }
            else
            {
                buffer.append(listSymbol);
            }

            if (propertyObject.isTable())
            {
                buffer.newLine();
                writeYamlTableLayer(propertyObject.asTable(), currentIndent + 1, buffer);
            }
            else if (propertyObject.hasValue())
            {
                var propertyObjectValue = propertyObject.asValue();
                var propertyObjectValueAsString = (String) null;

                if (propertyObject.isNum())
                {
                    propertyObjectValueAsString = StringUtils.toNumberString(propertyObject.asNum().getValue());
                }
                else
                {
                    propertyObjectValueAsString = String.valueOf(propertyObjectValue.getValue());
                }

                if (propertyObject.isString())
                {
                    if (propertyObjectValueAsString.contains("'"))
                    {
                        propertyObjectValueAsString = "\"" + propertyObjectValueAsString.replace("\"", "\\\"") + "\"";
                    }
                    else
                    {
                        propertyObjectValueAsString = "'" + propertyObjectValueAsString.replace("'", "\\'") + "'";
                    }
                }

                buffer.append(propertyObjectValueAsString);

                buffer.append("\n");
            }
        }

        buffer.newLine();
    }

    public ITableProperty readYamlTable(InputStream yamlStream)
    {
        String str = null;

        try
        {
            str = StreamsUtils.readAll("UTF-8", yamlStream).concat("\n");

            if (StringUtils.isBlank(str))
                return new KeyedTableProperty();

            var parser = new YamlDecoder(new StringReader(str));
            var object = parser.readObject();
            var swconfObject = javaToPropertyConverter.toProperty(object);
            return swconfObject.asTable();
        }
        catch (Throwable e)
        {
            throw new PropertyIOException("While parsing yaml text:\n*-*-*-*-*-*-*-*-*-Start-Of-File-*-*-*-*-*-*-*-*-*\n" + str + "\n*-*-*-*-*-*-*-*-*-End-Of-File-*-*-*-*-*-*-*-*-*", e);
        }
    }
}
