package ru.swayfarer.swl3.serialization.io;

public class PropertyIOException extends RuntimeException
{
    public PropertyIOException()
    {
    }

    public PropertyIOException(String message)
    {
        super(message);
    }

    public PropertyIOException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public PropertyIOException(Throwable cause)
    {
        super(cause);
    }
}
