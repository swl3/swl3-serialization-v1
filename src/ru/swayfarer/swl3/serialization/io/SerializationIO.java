package ru.swayfarer.swl3.serialization.io;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.exception.ExceptionsUtils;
import ru.swayfarer.swl3.funs.ExtendedOptional;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.io.streams.DataInStream;
import ru.swayfarer.swl3.io.streams.DataOutStream;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.observable.IObservable;
import ru.swayfarer.swl3.observable.Observables;
import ru.swayfarer.swl3.serializarion.property.ITableProperty;
import ru.swayfarer.swl3.serializarion.property.KeyedTableProperty;

@Getter
@Setter
@Accessors(chain = true)
public class SerializationIO
{
    @Internal
    @NonNull
    public IObservable<PropertyIOEvent<DataInStream>> eventReadFromIO = newObservable();

    @Internal
    @NonNull
    public IObservable<PropertyIOEvent<DataOutStream>> eventStoreToIO = newObservable();

    public ExtendedOptional<ITableProperty> readIO(IFunction0<DataInStream> in, String resource)
    {
        var propertyIOEvent = PropertyIOEvent.<DataInStream>builder()
                .dataStream(in)
                .resourceName(resource)
                .build()
        ;

        eventReadFromIO.next(propertyIOEvent);

        if (!propertyIOEvent.isCompleted())
            return ExtendedOptional.empty();

        return ExtendedOptional.of(propertyIOEvent.getTablePropertyObject());
    }

    public void storeIO(IFunction0<DataOutStream> dos, String resource, ITableProperty tableProperty)
    {
        System.out.println(tableProperty.toFancyString());

        var propertyIOEvent = PropertyIOEvent.<DataOutStream>builder()
                .tablePropertyObject(tableProperty)
                .dataStream(dos)
                .resourceName(resource)
                .build()
        ;

        eventStoreToIO.next(propertyIOEvent);

        ExceptionsUtils.IfNot(propertyIOEvent.isCompleted(), PropertyIOException.class, "Property", tableProperty, "not stored to", resource);
    }

    public SerializationIO addProvider(IPropertyIOProvider provider)
    {
        this.eventReadFromIO.subscribe().by(provider::read);
        this.eventStoreToIO.subscribe().by(provider::write);

        return this;
    }

    public <T> IObservable<PropertyIOEvent<T>> newObservable()
    {
        return Observables.<PropertyIOEvent<T>>createObservable()
                .execution().useFilter((exec) -> !exec.getEventObject().isCompleted())
                .exceptions().configure((eh) -> {
                    eh.configure()
                            .rule().any()
                                .wrap(PropertyIOException.class)
                                .thanSkip()
                                .throwEx()
                    ;
                })
        ;
    }
}
