package ru.swayfarer.swl3.serialization.io;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.experimental.SuperBuilder;
import ru.swayfarer.swl3.funs.GeneratedFuns;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.observable.event.AbstractEvent;
import ru.swayfarer.swl3.serializarion.property.ITableProperty;

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public class PropertyIOEvent<Stream_Type> extends AbstractEvent
{
    @Internal
    public ITableProperty tablePropertyObject;

    @Internal
    @NonNull
    public String resourceName;

    @Internal
    @NonNull
    public IFunction0<Stream_Type> dataStream;

    @Internal
    @NonNull
    public boolean completed;
}
